<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

   /*  public function beforeFilter() {
     
        //$this->Auth->allow('landing','juego');
           
    }*/
    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
    public function landing()
    {  
        $this->viewBuilder()->layout("frontend");
      
    }
    public function empresa()
    {  
        $this->viewBuilder()->layout("empresa");
  
    }
     public function juego()
    {  
        $this->viewBuilder()->layout("juego");
        
        $palabra = $this->request->session()->read('juego.palabras');
         $level= $this->request->session()->read( 'juego.level');
        if(empty( $palabra)){
            $palabra ="profesional";
            $level =1;
        }
         $this->set('palabras', $palabra);
         $this->set('level', $level);
       

    }
    public function juegolevel()
    {
            $this->autoRender = false;

            $palabras = array(
                                '1' => "profesional",
                                '2' => "curriculum",
                                '3' => "imagen",
                                '4' => "actualizado",
                                '5' => "ortografia",
                                '6' => "fortaleza",
                                '7' => "fotografia",
                                '8' => "sincero",
                                '9' => "conocimientos",
                                '10' => "educacion",
                                '11' => "experiencia",
                                '12' => "concreto",
                                '13' => "competencia",
                                '14' => "datos",
                                '15' => "correo",
                                '16' => "trabajo",
                                '17' => "telefono",
                                '18' => "personal",
                                '19' => "direccion",
                                '20' => "simple",
                                '21' => "seleccion",
                                '22' => "cronologico",
                                '23' => "referencia",
                                '24' => "busqueda",
                                '25' => "orden"

                             );

          if ($this->request->is('post')) {

          

             $level =  $this->request->data['level'] +1;

             $this->request->session()->write( 'juego.palabras',$palabras[ $level]);
             $this->request->session()->write( 'juego.level',$level);
               
             
        }
        
    }
}
