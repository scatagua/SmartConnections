<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function autorizadmin($role = null)
    {
      

        if ($role == "admin"){
            return 1;
        } else{
            return $this->redirect('Users/login');
        }

    }

    public function adminempresa($role = null)
    {
       if ($role == "admin" or $role == "admin" ){
            return 1;
        } else{
            return $this->redirect('Users/login');
        }

    }

       public function adminetalento($role = null)
    {
       if ($role == "admin" or $role == "user" ){
            return 1;
        } else{
            return $this->redirect('Users/login');
        }

    }

    public function index()
    {
          
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);
        $users = $this->paginate($this->Users,[ 'conditions' => [ 'role !=' => 'user' ]]);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);


    }

    public function login(){


            $this->viewBuilder()->layout( "LayoutLogin");

            if ($this->Auth->user()) {

                $role =  $this->request->session()->read('Auth.User.role') ;
                    switch ($role) {
                      case 'empresa':
                         return $this->redirect('Pages/empresa');
                        break;
                      case 'user':                    
                         return $this->redirect('Pages/landing');
                        break;
                        
                      default:
                        
                        return $this->redirect('Users/index');
                        break;
                    }

            }else{
               if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                     $role =  $this->request->session()->read('Auth.User.role') ;
                    switch ($role) {
                      case 'empresa':
                         return $this->redirect('Pages/empresa');
                        break;
                      case 'user':                    
                         return $this->redirect('Pages/landing');
                        break;
                        
                      default:
                        
                        return $this->redirect('Users/index');
                        break;
                    }
                   

                }
                $this->Flash->error(__('Invalid username or password, try again'));
            }

        }
    }
     

      public function logout()
  {
     $this->autoRender = false;
      return $this->redirect($this->Auth->logout());
  }

  
    public function view($id = null)
  {
       $role =  $this->request->session()->read('Auth.User.role') ;
      $this->autorizadmin($role);

      $user = $this->Users->get($id, [
          'contain' => []
      ]);

      $this->set('user', $user);
      $this->set('_serialize', ['user']);
  }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
          $this->request->data['active'] = 1;
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function registro()
    {


      $this->viewBuilder()->layout( "LayoutLogin");

            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {
              $this->request->data['role'] = 'admin';
                      $this->request->data['active'] = 1;
                $user = $this->Users->patchEntity($user, $this->request->data);

              if ($result=$this->Users->save($user)) {
              
                $authUser = $this->Users->get($result->id)->toArray();

                // Log user in using Auth
                $this->Auth->setUser($authUser);

                 $role =  $this->request->session()->read('Auth.User.role') ;
                      switch ($role) {
                        case 'empresa':
                           return $this->redirect('Pages/empresa');
                          break;
                        case 'user':                    
                           return $this->redirect('Pages/landing');
                          break;
                          
                        default:
                          
                          return $this->redirect('Users/index');
                          break;
                      }              
                  
              }
                $this->Flash->error(__('Su registro no pudo ser realizado. Por favor intente nuevamente'));
            }
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        
    }


    public function RegistroTalento()
    {
       

       $this->viewBuilder()->layout( "LayoutLogin");
      $user = $this->Users->newEntity();
      if ($this->request->is('post')) {

          $this->request->data['role'] = 'user';
          $this->request->data['active'] = 1;
          $this->request->data['step'] = 1;
            $user = $this->Users->patchEntity($user, $this->request->data);
            // var_dump($user); 

            if ($result=$this->Users->save($user)) {
              
              $authUser = $this->Users->get($result->id)->toArray();

              // Log user in using Auth
              $this->Auth->setUser($authUser);

               $role =  $this->request->session()->read('Auth.User.role') ;
                    switch ($role) {
                      case 'empresa':
                         return $this->redirect('Pages/empresa');
                        break;
                      case 'user':                    
                         return $this->redirect('Pages/landing');
                        break;
                        
                      default:
                        
                        return $this->redirect('Users/index');
                        break;
                    }              
                
            }
            $this->Flash->error(__('Su registro no pudo ser realizado. Por favor intente nuevamente'));
        }

    }
     public function UpdateTalento($id = null)
    {
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->adminetalento($role);

      $this->viewBuilder()->layout( "LayoutLogin");
      $user = $this->Users->get($id, [
            'contain' => []
        ]);
      
   
     if ($this->request->is('post')) {

            $name = $this->request->data['cv_file']['name'];
            $filesize = $this->request->data['cv_file']["size"];
            $file_ext = substr($name, strripos($name, '.'));
            $allowed_file_types = array('.doc','.docx','.pdf');  


            $name = $this->request->data['cv_file']['name'];
            $tmp_name = $this->request->data['cv_file']['tmp_name'];
            $filename = WWW_ROOT."file/cv/".preg_replace('[\s+]','', "id-".$id.$name);
            move_uploaded_file($tmp_name,$filename);

            chmod( $filename , 0777); 
             $this->request->data['step'] = 6;
            $this->request->data['url_cv'] = $filename;

            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
              $this->Flash->success(__('The user has been saved.'));

              return $this->redirect(['action' => 'index']);
            }
              $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
      $this->set('_serialize', ['user']);
    }

    public function talentos ()
    {
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->adminempresa($role);
        $this->paginate = [
        'limit' => 1000000000000000
        ];
        $users = $this->paginate($this->Users,[ 'conditions' => [ 'role' => 'user' ]] );

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);

    }

     public function talentosbyall ()
    {
         $role =  $this->request->session()->read('Auth.User.role') ;
          $this->adminempresa($role);
     if ($this->request->is('post')) { 

        $Universidad = $this->request->data['universidad'];
        $Edad = $this->request->data['edad'];
        $Estado =$this->request->data['estado'];
        $carrera =$this->request->data['carrera'];

        $this->paginate = ['limit' => 1000000000000000 ];

        $users = $this->paginate($this->Users,[ 'conditions' => [ 'role' => 'user',' YEAR( CURDATE( ) ) - YEAR(  `fecha_nacimiento` )= ' => $Edad,'Universidad' => $Universidad,'estado' => $Estado,'carrera' => $carrera ]] );

        $this->set(compact('users'));


     }else{

        $this->Flash->error(__('Disculpe pero los datos son incorrectos'));
     }

    }


    /* kpis */

    public function kpis()
    {
       $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);
        
      $totalUsers = $this->Users->find('all', [
          'conditions' => [ 'role' => 'user']
      ]);
      
      $talentos = $totalUsers->count();

      $totalFinish = $this->Users->find('all', [
          'conditions' => [ 'role' => 'user', 'step' => '6']
      ]);
      
      $Finish = $totalFinish->count();

      for ($i=2; $i < 7; $i++) { 

            $steps = $this->Users->find('all', [
            'conditions' => [ 'role' => 'user', 'step' => $i]
            ]);
       $pasos[$i] = $steps->count();
      }

     

      $edad = $this->Users->find('all', [
          'conditions' => [ ' YEAR( CURDATE( ) ) - YEAR(  `fecha_nacimiento` ) < ' => '30']
      ]);

       $edadcount = $edad->count();

       $this->set('talentos',$talentos);
       $this->set('edadcount',$edadcount);
       $this->set('pasos',$pasos);
       $this->set('Finish',$Finish);

    }

    public function steps()
    {
         

       
        if ($this->request->is(['patch', 'post', 'put'])) {
            
              $id =  $this->request->session()->read('Auth.User.id') ;
            $user = $this->Users->get($id, [
            'contain' => []
            ]);

            if ($user->step < $this->request->data['step']) {
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->request->session()->write( 'Auth.User.step',$this->request->data['step']);
                  
                  echo "ok";

                }else{
                     echo "no";
                }
            }
          
           echo "Listo";
           
        }

    }


    public function forgotPassword($email = null){
         $this->viewBuilder()->layout( "LayoutLogin");

          
        if($this->request->is('post')) {
             $email = $this->request->data['email'];

            
             $user = $this->Users->find()->where(['email' => $email ])->first();
             echo $user->first_name;
             if (!$user) {
                 $this->Flash->error(__('Este usuario no existe'));
                 return $this->redirect(['controller' => 'Users','action' => 'forgotPassword']);

            }else{

                    
                    $val = $this->generateRandomString();
                    $this->request->data['password'] = $val;
                    $user = $this->Users->patchEntity($user, $this->request->data);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('se envío un correo a su dirección '));
                        $email = new Email();
                        $email
                        ->template('default')
                        ->emailFormat('html')
                        ->to($user->email)
                        ->from('webadmin@nestle.com')
                        ->viewVars(['clave' => $val, 'user' => $user->Username, 'nombre' => $user->first_name])
                        ->send();

                        return $this->redirect(['action' => 'forgotPassword']);
                    }

            }
        }

    }


   public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

      }

}


