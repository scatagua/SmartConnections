        <!-- barra de estado -->
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/" class="site_title"></i> <span> Panel Administrativo</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
              <?= $this->Html->image("img.jpg", ['class' => 'img-circle profile_img']); ?>
             
              </div>
              <div class="profile_info">
                <span>Usuario</span>
                <h2><?php echo $role =  $this->request->session()->read('Auth.User.role') ; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                <?php 
                
                 if ($role == "admin") { ?>

               
                
                  <li> 
                        <?php
                          echo $this->Html->link(
                           '<i class="fa fa-user-circle"></i> Usuarios',
                            array(
                                'controller'=>'users',
                                'action'=>'index'                       
                            ),
                            array('escape'=> false )
                          );
                        ?> 
                  </li>
                   <li>                   
                        <?php
                          echo $this->Html->link(
                           '<i class="fa fa-id-card-o"></i> Talento',
                            array(
                                'controller'=>'users',
                                'action'=>'talentos'                       
                            ),
                            array('escape'=> false )
                          );
                        ?> 
                  </li>
                  <li>                    
                    <?php
                          echo $this->Html->link(
                           '<i class="fa fa-bar-chart-o"></i> KPIS',
                            array(
                                'controller'=>'users',
                                'action'=>'kpis'                       
                            ),
                            array('escape'=> false )
                          );
                        ?> 

                  </li>
                  <?php  }else{ ?>

                   <li>                   
                        <?php
                          echo $this->Html->link(
                           '<i class="fa fa-id-card-o"></i> Talento',
                            array(
                                'controller'=>'users',
                                'action'=>'talentos'                       
                            ),
                            array('escape'=> false )
                          );
                        ?> 
                  </li>

                  <?php }    ?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          
            <!-- /menu footer buttons -->
          </div>
        </div>
        <!--/ barra de estado  -->