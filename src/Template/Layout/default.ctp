<?php

$cakeDescription = 'Sistema de conexion con mercadolibre';
?>

<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <?= $this->Html->charset() ?>
        <?= $this->Html->meta('icon') ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>      

        <!-- Bootstrap -->
          <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>        
        <!-- Font Awesome -->
        <?= $this->Html->css('/vendors/font-awesome2/css/font-awesome.css') ?>                
        <!-- jQuery custom content scroller -->
          <?= $this->Html->css('/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') ?>    
        <!-- Custom Theme Style -->
          <?= $this->Html->css('/build/css/custom.min.css') ?>  

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
             <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>    
          <!-- jQuery -->
        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js') ?>    

      </head>

    <body class="nav-md">
        <div class="container body">
                <?= $this->Flash->render() ?>
            <div class="main_container">
                <?= $this->element('bar')?>
                <?= $this->element('navigation')?>
                <?= $this->fetch('content') ?>
            </div>
            <footer>
                <div class="pull-right">
                  
                </div>
                  <div class="clearfix"></div>
            </footer>
        </div>


        
        <!-- Bootstrap -->
        <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>    

        <!-- FastClick -->
        <?= $this->Html->script('/vendors/fastclick/lib/fastclick.js') ?>    

        <!-- NProgress -->
        <?= $this->Html->script('/vendors/nprogress/nprogress.js') ?>   

        <!-- jQuery custom content scroller -->
        <?= $this->Html->script('/vendors/malihu-custom-scrollbar-plugin/query.mCustomScrollbar.concat.min.js') ?>  

        <!-- vista Custom  -->
        <?= $this->fetch('script') ?>   
        <!-- Custom Theme Scripts -->
        <?= $this->Html->script('/build/js/custom.min.js') ?>   

    
    </body>
</html>
