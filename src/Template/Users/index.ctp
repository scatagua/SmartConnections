
        
       <?= $this->Html->css('/vendors/iCheck/skins/flat/green.css') ?>  

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Lista de Usuarios <small><strong></strong></small></h3>
              </div>
            </div>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabla Usuarios <small>Tabla Usuarios</small></h2>
                 
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                  
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>

                            <th class="column-title">id </th>
                            <th class="column-title">Nombre </th>
                            <th class="column-title">Apellido </th>
                            <th class="column-title">Email </th>
                            <th class="column-title">Rol </th>
                            <th class="column-title">Creado </th>
                            <th class="column-title no-link last"><span class="nobr">Actiones</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($users as $user): ?>

                            <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" "><?= $this->Number->format($user->id) ?></td>
                            <td class=" "><?= h($user->first_name) ?></td>
                            <td class=" "><?= h($user->last_name) ?></td>
                            <td class=" "><?= h($user->email) ?></td>
                          
                            <td class=" "><?= h($user->role) ?></td>
                            <td class="a-right a-right "><?= h($user->created) ?></td>
                            <td class=" last">
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id]) ?>
                                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id], ['confirm' => __('Usted   realmente quiere eliminar el usuario # {0}?', $user->id)]) ?>
                            </td>
                          </tr>
                           
                        <?php endforeach; ?>
                            <?php
                            echo $this->Html->link('Agregar Usuarios','/users/add   ',['class' => 'btn btn-success']);
                            ?>
                          
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!-- /page content -->

