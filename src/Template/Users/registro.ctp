<!-- <div class="container">
    <div class="row agradecimiento">
        <div class="col-12 align-self-center text-center">
            <img class="img-fluid" src="img/registro/logo-small.png">
            <h1>Gracias<br> por Registrarte
            </h1>
            <h3>En este portal de Iniciativa por los jóvenes <strong>encontrarás e-learning o capacitaciones que te brindarán la oportunidad de subir tu CV</strong> y que Nestlé y sus aliados puedan contactarte</h3>
        </div>
    </div>
</div> -->
<div class="container">

    <form action="/users/registro" enctype='multipart/form-data' method="post">
        <div class="row">
            <div class="col-12 col-md-6 p-5 align-self-stretch d-block first-col-login">
                <div class="form-group">
                    <label for="">Nombres:</label> <input name="first_name" class="form-control" id="" type="text" required>
                </div>
                <div class="form-group">
                    <label for="">Apellidos:</label> <input name="last_name" class="form-control" id="" type="text" required>
                </div>
                <div class="form-group">
                    <label for="">Carrera/Profesión:</label> <select name="carrera" class="form-control" id="" required>
                        <option selected>Seleccione</option>
                        <option value="Abogacía">Abogacía</option>
                        <option value="Actuaría">Actuaría</option>
                        <option value="Acuicultura">Acuicultura</option>
                        <option value="Adm. De Empresas">Adm. De Empresas</option>
                        <option value="Adm y Gestión Pública">Adm y Gestión Pública</option>
                        <option value="Adm Agropecuaria">Adm Agropecuaria</option>
                        <option value="Agrimensor">Agrimensor</option>
                        <option value="Agronegocios">Agronegocios</option>
                        <option value="Antropología">Antropología</option>
                        <option value="Análisis de Sistemas">Análisis de Sistemas</option>
                        <option value="Apoderado Aduanal">Apoderado Aduanal</option>
                        <option value="Arqueología">Arqueología</option>
                        <option value="Asesoría Legal Internacional">Asesoría Legal Internacional</option>
                        <option value="Asesoría Comercio Exterior">Asesoría Comercio Exterior</option>
                        <option value="Astronomía">Astronomía</option>
                        <option value="Bellas Artes">Bellas Artes</option>
                        <option value="BioFísica">BioFísica</option>
                        <option value="BioIngeniería">BioIngeniería</option>
                        <option value="Bliología">Bliología</option>
                        <option value="Bioquímica">Bioquímica</option>
                        <option value="Biotecnología">Biotecnología</option>
                        <option value="Call Centes">Call Centes</option>
                        <option value="Cartografía">Cartografía</option>
                        <option value="Ciencias Físicas">Ciencias Físicas</option>
                        <option value="Ciencias Políticas">Ciencias Políticas</option>
                        <option value="Ciencias de la Educación">Ciencias de la Educación</option>
                        <option value="Comercio Int/Ext">Comercio Int/Ext</option>
                        <option value="Computación / Informática">Computación / Informática</option>
                        <option value="Comunciación Audiovisual">Comunciación Audiovisual</option>
                        <option value="Comunicación Social">Comunicación Social</option>
                        <option value="Construcción /Obras civiles">Construcción /Obras civiles</option>
                        <option value="Contabilidad / Auditoría">Contabilidad / Auditoría</option>
                        <option value="Dibujo Técnico">Dibujo Técnico</option>
                        <option value="Diseño Gráfico">Diseño Gráfico</option>
                        <option value="Diseño Industrial">Diseño Industrial</option>
                        <option value="Diseño Web">Diseño Web</option>
                        <option value="Ecología">Ecología</option>
                        <option value="Economía">Economía</option>
                        <option value="Educación">Educación</option>
                        <option value="Electricidad">Electricidad</option>
                        <option value="Electrónica">Electrónica</option>
                        <option value="Enfermería">Enfermería</option>
                        <option value="Enología">Enología</option>
                        <option value="Estadística">Estadística</option>
                        <option value="Farmacia">Farmacia</option>
                        <option value="Filosofía">Filosofía</option>
                        <option value="Finanzas">Finanzas</option>
                        <option value="Fisioterapia">Fisioterapia</option>
                        <option value="Fotografía">Fotografía</option>
                        <option value="Gastronomía">Gastronomía</option>
                        <option value="Geofísica">Geofísica</option>
                        <option value="Geología">Geología</option>
                        <option value="Hidráulica">Hidráulica</option>
                        <option value="Historia">Historia</option>
                        <option value="Hotelería">Hotelería</option>
                        <option value="Ing. Aerospacial">Ing. Aerospacial</option>
                        <option value="Ing. Agropecuario">Ing. Agropecuario</option>
                        <option value="Ing. Agrónomo">Ing. Agrónomo</option>
                        <option value="Ing. Alimentos">Ing. Alimentos</option>
                        <option value="Ing. Ambiental">Ing. Ambiental</option>
                        <option value="Ing. Comercial">Ing. Comercial</option>
                        <option value="Ing. Electrónica">Ing. Electrónica</option>
                        <option value="Ing. Eléctrica">Ing. Eléctrica</option>
                        <option value="Ing. Forestal">Ing. Forestal</option>
                        <option value="Ing. Hidáulica">Ing. Hidáulica</option>
                        <option value="Ing. Industrial">Ing. Industrial</option>
                        <option value="Ing. Informática">Ing. Informática</option>
                        <option value="Ing. Matemática">Ing. Matemática</option>
                        <option value="Ing. Mecánica">Ing. Mecánica</option>
                        <option value="Ing. Naval">Ing. Naval</option>
                        <option value="Ing. Obras Civiles / Construcciones">Ing. Obras Civiles / Construcciones</option>
                        <option value="Ing. Química">Ing. Química</option>
                        <option value="Ing. Recursos Hídricos">Ing. Recursos Hídricos</option>
                        <option value="Ing. Sonido">Ing. Sonido</option>
                        <option value="Ing. Telecomunicaciones">Ing. Telecomunicaciones</option>
                        <option value="Ing. Transporte">Ing. Transporte</option>
                        <option value="Ing. Materiales">Ing. Materiales</option>
                        <option value="Ing. Sistemas">Ing. Sistemas</option>
                        <option value="Ing. Vial">Ing. Vial</option>
                        <option value="Intérprete">Intérprete</option>
                        <option value="Kinesiología">Kinesiología</option>
                        <option value="Laboratorio">Laboratorio</option>
                        <option value="Literatura">Literatura</option>
                        <option value="Marketing">Marketing</option>
                        <option value="Matemáticas">Matemáticas</option>
                        <option value="Mecánica">Mecánica</option>
                        <option value="Medicina">Medicina</option>
                        <option value="Medio Ambiente">Medio Ambiente</option>
                        <option value="Mercadotecnia ">Mercadotecnia</option>
                        <option value="Nutrición">Nutrición</option>
                        <option value="Odontología">Odontología</option>
                        <option value="Periodismo">Periodismo</option>
                        <option value="Programación">Programación</option>
                        <option value="Psicología">Psicología</option>
                        <option value="Psicopedagogía">Psicopedagogía</option>
                        <option value="Publicidad">Publicidad</option>
                        <option value="Química">Química</option>
                        <option value="Recursos Humanos /Relaciones Industriales">Recursos Humanos /Relaciones Industriales</option>
                        <option value="Relaciones Internacionales">Relaciones Internacionales</option>
                        <option value="Relaciones Publicas">Relaciones Publicas</option>
                        <option value="Secretariado">Secretariado</option>
                        <option value="Seguridad Industrial">Seguridad Industrial</option>
                        <option value="Seguros">Seguros</option>
                        <option value="Sociología">Sociología</option>
                        <option value="Técnico">Técnico</option>
                        <option value="Turismo">Turismo</option>
                        <option value="Ventas">Ventas</option>
                        <option value="Veterinaria">Veterinaria</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Fecha de nacimiento:</label> <input name="fecha_nacimiento" class="form-control datepicker" id="birthday-date" type="text" required>
                </div>
                <div class="form-group">
                    <label for="">Estado:</label> <select name="estado" class="form-control" id="" required>
                        <option selected>Seleccione</option>
                        <option value="Amazonas">Amazonas</option>
                        <option value="Anzoátegui">Anzoátegui</option>
                        <option value="Apure">Apure</option>
                        <option value="Aragua">Aragua</option>
                        <option value="Barinas">Barinas</option>
                        <option value="Bolívar">Bolívar</option>
                        <option value="Carabobo">Carabobo</option>
                        <option value="Cojedes">Cojedes</option>
                        <option value="Delta Amacuro">Delta Amacuro</option>
                        <option value="Dependencias Federales">Dependencias Federales</option>
                        <option value="Distrito Capital">Distrito Capital</option>
                        <option value="Falcón">Falcón</option>
                        <option value="Guárico">Guárico</option>
                        <option value="Lara">Lara</option>
                        <option value="Mérida">Mérida</option>
                        <option value="Miranda">Miranda</option>
                        <option value="Monagas">Monagas</option>
                        <option value="Nueva Esparta">Nueva Esparta</option>
                        <option value="Portuguesa">Portuguesa</option>
                        <option value="Sucre">Sucre</option>
                        <option value="Táchira">Táchira</option>
                        <option value="Trujillo">Trujillo</option>
                        <option value="Vargas">Vargas</option>
                        <option value="Yaracuy">Yaracuy</option>
                        <option value="Zulia">Zulia</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Universidad:</label> <select name="universidad" class="form-control" id="" required>
                        <option selected>Seleccione</option>
                        <option value="Colegios Universitarios">Colegios Universitarios</option>
                        <option value="Colegio Universitario de Caracas">Colegio Universitario de Caracas</option>
                        <option value="Colegio Universitario de Enfermería de la Alcaldía Metropolitana de Caracas ">Colegio Universitario de Enfermería de la Alcaldía Metropolitana de Caracas </option>
                        <option value="Colegio Universitario de Rehabilitación May Hamilton ">Colegio Universitario de Rehabilitación May Hamilton </option>
                        <option value="Colegio Universitario Francisco de Miranda ">Colegio Universitario Francisco de Miranda </option>
                        <option value="Colegio Universitario Hotel Escuela Los Andes Venezolanos ">Colegio Universitario Hotel Escuela Los Andes Venezolanos </option>
                        <option value="Colegio Universitario Prof. José Lorenzo Pérez ">Colegio Universitario Prof. José Lorenzo Pérez </option>
                        <option value="Institutos Universitarios">Institutos Universitarios</option>
                        <option value="Instituto Militar Universitario de Tecnología de la Guardia Nacional ">Instituto Militar Universitario de Tecnología de la Guardia Nacional </option>
                        <option value="Instituto Universitario de Aeronáutica Civil Mayor AV Miguel Rodríguez">Instituto Universitario de Aeronáutica Civil Mayor AV Miguel Rodríguez</option>
                        <option value="Instituto Universitario del Oeste Mariscal Sucre">Instituto Universitario del Oeste Mariscal Sucre</option>
                        <option value="Instituto Universitario de Tecnología Agroindustrial ">Instituto Universitario de Tecnología Agroindustrial </option>
                        <option value="Instituto Universitario de Tecnología de Cabimas ">Instituto Universitario de Tecnología de Cabimas </option>
                        <option value="Instituto Universitario de Tecnología del Estado Bolívar ">Instituto Universitario de Tecnología del Estado Bolívar </option>
                        <option value="Instituto Universitario de Tecnología de los Llanos ">Instituto Universitario de Tecnología de los Llanos </option>
                        <option value="Instituto Universitario de Tecnología de Maracaibo">Instituto Universitario de Tecnología de Maracaibo</option>
                        <option value="Instituto Universitario de Tecnología de Puerto Cabello ">Instituto Universitario de Tecnología de Puerto Cabello </option>
                        <option value="Instituto Universitario de Tecnología de Valencia ">Instituto Universitario de Tecnología de Valencia </option>
                        <option value="Instituto Universitario de Tecnología DR. Federico Rivero Palacios ">Instituto Universitario de Tecnología DR. Federico Rivero Palacios </option>
                        <option value="Instituto Universitario de Tecnología Escuela Nacional de Administración y Hacienda Publica ">Instituto Universitario de Tecnología Escuela Nacional de Administración y Hacienda Publica </option>
                        <option value="Instituto Universitario Latinoamericano de Agroecologia Paulo Freire ">Instituto Universitario Latinoamericano de Agroecologia Paulo Freire </option>
                        <option value="Universidades Experimentales ">Universidades Experimentales </option>
                        <option value="Universidad Bolivariana de Trabajadores Jesús Rivero ">Universidad Bolivariana de Trabajadores Jesús Rivero </option>
                        <option value="Universidad Bolivariana de Venezuela ">Universidad Bolivariana de Venezuela </option>
                        <option value="Universidad Campesina de Venezuela Argimiro Gabaldón ">Universidad Campesina de Venezuela Argimiro Gabaldón </option>
                        <option value="Universidad Centro Occidental Lisandro Alvarado ">Universidad Centro Occidental Lisandro Alvarado </option>
                        <option value="Universidad de Ciencias de la Salud ">Universidad de Ciencias de la Salud </option>
                        <option value="Universidad Deportiva del Sur ">Universidad Deportiva del Sur </option>
                        <option value="Universidad Latinoamericana del Caribe ">Universidad Latinoamericana del Caribe </option>
                        <option value="Universidad Militar Bolivariana de Venezuela ">Universidad Militar Bolivariana de Venezuela </option>
                        <option value="Universidad Nacional Abierta ">Universidad Nacional Abierta </option>
                        <option value="Universidad Nacional Experimental de Guayana ">Universidad Nacional Experimental de Guayana </option>
                        <option value="Universidad Nacional Experimental de la Fuerza Armada ">Universidad Nacional Experimental de la Fuerza Armada </option>
                        <option value="Universidad Nacional Experimental de las Artes ">Universidad Nacional Experimental de las Artes </option>
                        <option value="Universidad Nacional Experimental de la Seguridad ">Universidad Nacional Experimental de la Seguridad </option>
                        <option value="Universidad Nacional Experimental de Los Llanos Centrales Rómulo Gallegos ">Universidad Nacional Experimental de Los Llanos Centrales Rómulo Gallegos </option>
                        <option value="Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora ">Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora </option>
                        <option value="Universidad Nacional Experimental del Táchira ">Universidad Nacional Experimental del Táchira </option>
                        <option value="Universidad Nacional Experimental de Yaracuy ">Universidad Nacional Experimental de Yaracuy </option>
                        <option value="Universidad Nacional Experimental Francisco de Miranda ">Universidad Nacional Experimental Francisco de Miranda </option>
                        <option value="Universidad Nacional Experimental Indígena del Tauca ">Universidad Nacional Experimental Indígena del Tauca </option>
                        <option value="Universidad Nacional Experimental Marítima del Caribe ">Universidad Nacional Experimental Marítima del Caribe </option>
                        <option value="Universidad Nacional Experimental Politécnica Antonio José de Sucre ">Universidad Nacional Experimental Politécnica Antonio José de Sucre </option>
                        <option value="Universidad Nacional Experimental Rafael María Baralt ">Universidad Nacional Experimental Rafael María Baralt </option>
                        <option value="Universidad Nacional Experimental Simón Bolívar ">Universidad Nacional Experimental Simón Bolívar </option>
                        <option value="Universidad Nacional Experimental Simón Rodríguez ">Universidad Nacional Experimental Simón Rodríguez </option>
                        <option value="Universidad Nacional Experimental Sur del Lago Jesús María Semprum ">Universidad Nacional Experimental Sur del Lago Jesús María Semprum </option>
                        <option value="Universidad Pedagógica Experimental Libertador ">Universidad Pedagógica Experimental Libertador </option>
                        <option value="Universidad Politécnica Experimental de Los Altos Mirandinos "Cecilio Acosta" ">Universidad Politécnica Experimental de Los Altos Mirandinos "Cecilio Acosta" </option>
                        <option value="Universidad Politecnica Territorial de Amazonas ">Universidad Politecnica Territorial de Amazonas </option>
                        <option value="Universidad Politécnica Territorial de Barlovento Argelia Laya ">Universidad Politécnica Territorial de Barlovento Argelia Laya </option>
                        <option value="Universidad Politécnica Territorial de Falcón Alonso Gamero ">Universidad Politécnica Territorial de Falcón Alonso Gamero </option>
                        <option value="Universidad Politécnica Territorial de La Guaira José María España ">Universidad Politécnica Territorial de La Guaira José María España </option>
                        <option value="Universidad Politécnica Territorial del Alto Apure Pedro Camejo ">Universidad Politécnica Territorial del Alto Apure Pedro Camejo </option>
                        <option value="Universidad Politécnica Territorial del Estado Aragua Federico Brito Figueroa ">Universidad Politécnica Territorial del Estado Aragua Federico Brito Figueroa </option>
                        <option value="Universidad Politécnica Territorial del estado Barinas José Félix Ribas ">Universidad Politécnica Territorial del estado Barinas José Félix Ribas </option>
                        <option value="Universidad Politécnica Territorial del Estado Lara Andrés Eloy Blanco ">Universidad Politécnica Territorial del Estado Lara Andrés Eloy Blanco </option>
                        <option value="Universidad Politécnica Territorial del Estado Mérida Kléber Ramírez ">Universidad Politécnica Territorial del Estado Mérida Kléber Ramírez </option>
                        <option value="Universidad Politécnica Territorial del Estado Trujillo Mario Briceño Iragorry ">Universidad Politécnica Territorial del Estado Trujillo Mario Briceño Iragorry </option>
                        <option value="Universidad Politécnica Territorial del Norte del Táchira "Manuela Sáenz" ">Universidad Politécnica Territorial del Norte del Táchira "Manuela Sáenz" </option>
                        <option value="Universidad Politécnica Territorial del Norte de Monagas Ludovico Silva ">Universidad Politécnica Territorial del Norte de Monagas Ludovico Silva </option>
                        <option value="Universidad Politécnica Territorial del Oeste del Estado Sucre Clodosbaldo Russian ">Universidad Politécnica Territorial del Oeste del Estado Sucre Clodosbaldo Russian </option>
                        <option value="Universidad Politécnica Territorial de los Valles del Tuy ">Universidad Politécnica Territorial de los Valles del Tuy </option>
                        <option value="Universidad Politécnica Territorial Deltaica Francisco Tamayo ">Universidad Politécnica Territorial Deltaica Francisco Tamayo </option>
                        <option value="Universidad Politécnica Territorial de Paria Luis Mariano Rivera ">Universidad Politécnica Territorial de Paria Luis Mariano Rivera </option>
                        <option value="Universidad Politécnica Territorial de Portuguesa JJ Montilla ">Universidad Politécnica Territorial de Portuguesa JJ Montilla </option>
                        <option value="Universidad Politécnica Territorial de Yaracuy Arístides Bastidas ">Universidad Politécnica Territorial de Yaracuy Arístides Bastidas </option>
                        <option value="Universidad Politécnica Territorial José Antonio Anzoátegui ">Universidad Politécnica Territorial José Antonio Anzoátegui </option>
                        <option value="Universidad Venezolana de Hidrocarburos ">Universidad Venezolana de Hidrocarburos </option>
                        <option value="Universidades Nacionales ">Universidades Nacionales </option>
                        <option value="Universidad Central de Venezuela ">Universidad Central de Venezuela </option>
                        <option value="Universidad de Carabobo ">Universidad de Carabobo </option>
                        <option value="Universidad de los Andes ">Universidad de los Andes </option>
                        <option value="Universidad del Zulia ">Universidad del Zulia </option>
                        <option value="Universidad de Oriente ">Universidad de Oriente </option>
                        <option value="Mision Sucre ">Mision Sucre </option>
                        <option value="Centro Docente Cardiológico Bolivariano Aragua ">Centro Docente Cardiológico Bolivariano Aragua </option>
                        <option value="Fundación Escuela Venezolana de Planificación ">Fundación Escuela Venezolana de Planificación </option>
                        <option value="Fundación Instituto de Estudios Avanzados ">Fundación Instituto de Estudios Avanzados </option>
                        <option value="Instituto de Altos Estudios Diplomáticos Pedro Gual ">Instituto de Altos Estudios Diplomáticos Pedro Gual </option>
                        <option value="Instituto de Altos Estudios en Salud Pública Dr. Arnoldo Gabaldón ">Instituto de Altos Estudios en Salud Pública Dr. Arnoldo Gabaldón </option>
                        <option value="Instituto Nacional de Higiene Rafael Rangel ">Instituto Nacional de Higiene Rafael Rangel </option>
                        <option value="Instituto Nacional de Investigaciones Agrícolas ">Instituto Nacional de Investigaciones Agrícolas </option>
                        <option value="Instituto Venezolano de Investigaciones Científicas ">Instituto Venezolano de Investigaciones Científicas </option>
                        <option value="otra">otra</option>
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-6 p-5 align-self-stretch d-block second-col-login">
                <div class="form-group">
                    <label for="">Usuario:</label> <input name="Username" class="form-control" id="" type="username" required>
                </div>
                <div class="form-group">
                    <label for="">Correo:</label> <input name="email" class="form-control" id="" type="email" required>
                </div>
                <div class="form-group">
                    <label for="">Contraseña:</label> <input name=" password" class="form-control" id="password" type="password" required>
                </div>
                <div class="form-group">
                    <label for="">Confirmar Contraseña:</label> <input class="form-control" id="confirm_password" type="password" required>
                </div>
        
                <button class="btn btn-primary mx-auto mt-5" type="submit">Registro</button>
            </div>
        </div>
    </form>
</div>