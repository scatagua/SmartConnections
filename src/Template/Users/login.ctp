<div class="container">
    <div class="row bienvenido">
        <div class="col-12 align-self-center text-center">
            <?php echo $this->Html->image("/frontend/img/registro/logo.png", ['class' => 'img-fluid']); ?>
            <h1>¡Regístrate!</h1>
            <h3>Aprenderás habilidades para conseguir <strong>tu primer empleo</strong></h3>
        </div>
    </div>
</div>
<div class="container registro">
    <div class="row">
        <div class="col-12 col-md-6 p-5 order-md-2 align-self-stretch align-items-center d-flex second-col-login">
            <form  method="post" accept-charset="utf-8" action="/users/login">
                <div class="form-group">
                    <label for="">Usuario:</label>
                    <input type="text" name="Username" class="form-control" id="">
                </div>
                <div class="form-group">
                    <label for="">Contraseña:</label>
                    <input type="password" name="password" class="form-control" id="">
                </div>
                 <p class="text-center"> <a href="/users/forgot_password" style="color:white;"> ¿Olvidaste tu cuenta? </a></p>
                <button type="submit" class="btn btn-primary mx-auto mt-5">Iniciar Sesión</button>
                <?= $this->Flash->render() ?>
            </form>
        </div>
        <div class="col-12 col-md-6 p-5 order-md-1 align-self-stretch align-items-center d-flex first-col-login">
            <a class="mx-auto" href="/users/registro "><button type="button" class="btn btn-primary">Registro</button></a>
        </div>
    </div>
</div>
