<!-- <div class="container">
    <div class="row agradecimiento">
        <div class="col-12 align-self-center text-center">
            <img class="img-fluid" src="img/registro/logo-small.png">
            <h1>Gracias<br> por Registrarte
            </h1>
            <h3>En este portal de Iniciativa por los jóvenes <strong>encontrarás e-learning o capacitaciones que te brindarán la oportunidad de subir tu CV</strong> y que Nestlé y sus aliados puedan contactarte</h3>
        </div>
    </div>
</div> -->
<div class="container">

    <form action="/users/forgot_password" enctype='multipart/form-data' method="post">
        <div class="row">

            <div class="col p-5 second-col-login">
                <div class="row">
                    <div class="col-12 col-md-6 d-block mx-auto" style="top: 39%;transform: translate(-50%);position: absolute;left: 50%;">
                        <div class="form-group">
                            <label for="">Email:</label>
                            <input name="email" class="form-control" id="" type="email">
                        </div>

                         <?= $this->Flash->render() ?>

                        <button class="btn btn-primary mx-auto mt-5" type="submit">Verificar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>