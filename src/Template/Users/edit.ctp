
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Lista de Usuarios <small><strong></strong></small></h3>
              </div>
            </div>
          </div>
            <div class="col-md-12 col-xs-8">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar Usuario<small></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

  
                    <?= $this->Form->create($user, ['class' => 'form-horizontal form-label-left input_mask']) ?>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $this->Form->input('first_name', ['class' => 'form-control', 'id' => 'inputSuccess2', 'placeholder' => 'First Name']);
                        ?>
                        
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $this->Form->input('last_name', ['class' => 'form-control', 'id' => 'inputSuccess3', 'placeholder' => 'Last Name']);
                        ?>
                      
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $this->Form->input('email', ['class' => 'form-control', 'id' => 'inputSuccess4', 'placeholder' => 'Email']);
                        ?>
                        
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $this->Form->input('Username', ['class' => 'form-control', 'id' => 'inputSuccess5', 'placeholder' => 'Nombre de Usuario']);?>
                        
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $this->Form->input('password', ['class' => 'form-control', 'id' => 'inputSuccess6', 'placeholder' => 'Password']);
                        ?>                        
                      </div>
                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php echo $this->Form->input('role', ['class' => 'form-control', 'id' => 'inputSuccess7', 'placeholder' => 'Role']);?>

                        </div>
                      </div>
                      <div class="form-group">                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php echo $this->Form->input('active', ['class' => 'form-control', 'id' => 'inputSuccess8', 'placeholder' => 'Active']);?>

                        </div>
                      </div>

                     <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        
                        
                        <?php
                        echo $this->Html->link('Regresar','/users   ',['class' => 'btn btn-success']);
                        ?>
                       
                          <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-success']) ?>
                        </div>
                      </div>


                      <?= $this->Form->end() ?>
                  </div>                
            </div>
</div>