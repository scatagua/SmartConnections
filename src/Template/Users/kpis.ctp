  <?= $this->Html->css('/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>        


  <div class="right_col" role="main">
        <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total usuarios</span>
              <div class="count"><?php echo $talentos?></div>
            </div>            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Talentos que han terminado los e-learning.</span>
              <div class="count"><?php echo $Finish?></div>
             
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Talentos con CV menores de 30 años</span>
              <div class="count green"><?php echo $edadcount?></div>
              
            </div>
            
          </div>
          <!-- /top tiles -->
          <div class="row">
             <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Reporte de abandono de los e-learning </h2>
                
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h4>Pasos donde han llegado los usuarios:</h4>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Paso 2</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?php ($pasos[2]*100)/ $talentos ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php ($pasos[2]*100)/ $talentos ?>%;">
                          <span class="sr-only"><?php echo ($pasos[2]*100)/ $talentos ?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                      <span><?php echo $pasos[2]." de ".$talentos ?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Paso 3</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($pasos[3]*100)/ $talentos ?>%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                       <span><?php echo $pasos[3]." de ".$talentos ?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Paso 4</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($pasos[4]*100)/ $talentos ?>%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                       <span><?php echo $pasos[4]." de ".$talentos ?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Paso 5</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($pasos[5]*100)/ $talentos ?>%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                       <span><?php echo $pasos[5]." de ".$talentos ?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Paso 6</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo (($pasos[6]*100)/ $talentos) ?>%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                      <span><?php echo $pasos[6]." de ".$talentos ?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>

                </div>
              </div>
            </div>
            
          </div>
</div>
   <?= $this->Html->script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>   