
<div class="container">
        
    <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand mx-center" href="#">
         <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
           
            </a>
            
            <div class="navbar-collapse flex-row-reverse" id="navbarNavAltMarkup">
                <span class="navbar-textfloat-lg-right">
                <?php 

                 if( $this->request->session()->read('Auth.User.role')){ ?>
                 <a style="display: block" href="/users/logout">
                    <?php echo $this->Html->image("/frontend/img/landing/cerrar-sesion.png", ['class' => 'img-fluid mt-4 mt-lg-0 mb-4 mb-lg-0 mx-center login-now']); ?>
                    </a>
                <?php }else{ ?>
                <a style="display: block" href="/users/login">
                    <?php echo $this->Html->image("/frontend/img/landing/iniciar-sesion.png", ['class' => 'img-fluid mt-4 mt-lg-0 mb-4 mb-lg-0 mx-center login-now']); ?>
                    </a>
                 <?php }   ?>
                    
                </span>
                <div class="navbar-nav justify-content-between justify-content-lg-start flex-row float-lg-right">
                    <a class="nav-item nav-link active" href="#second-section">Talento con gusto</a>
                    <a class="nav-item nav-link" href="/blog/">Noticias</a>
                    <a class="nav-item nav-link" href="#fifth-section">Contáctanos</a>
                </div>
            </div>
        </nav>
        
    </div>

    <!-- Main container -->

    <div class="container">
        <div id="first-section" class="row align-items-start align-items-md-center first-section">
         <?php echo $this->Html->image("/frontend/img/landing/banner-1.jpg", ['class' => 'img-fluid img-bg-1']); ?>
         <?php echo $this->Html->image("/frontend/img/landing/banner-1-mobile.png", ['class' => 'img-fluid img-bg-1-mobile']); ?>
              
            <div class="col-12 col-md-6 p-1 bg-first-section align-items-md-center d-flex">
                <p class="text-first-section">Contribuimos en la preparación de la próxima generación de trabajadores para Venezuela</p>
            </div>
            <div class="col-9 col-md-4 p-0 mx-auto text-center text-md-left">
                <a data-lity href="https://youtu.be/Ocnaw978CmQ">
                    <?php echo $this->Html->image("/frontend/img/landing/video.png", ['class' => 'video-play img-fluid']); ?>
                </a>
            </div>
            <div class="col-12 col-md-2">
            </div>
            <a href="#second-section" class="arrow-section arrow-section-1"></a>
        </div>
        <div id="second-section" class="row align-items-lg-center align-items-start  second-section">
         <?php echo $this->Html->image("/frontend/img/landing/banner-2.jpg", ['class' => 'img-fluid img-bg-2']); ?>
         <?php echo $this->Html->image("/frontend/img/landing/banner-2-mobile.jpg", ['class' => 'img-fluid img-bg-2']); ?>

            <div class="col-12 col-md-4 bg-second-section">
                <p class="text-second-section text-md-right text-center font-weight-bold">Talento con gusto</p>
                <p class="text-second-section text-md-right text-center">Completa los <strong>e-learning</strong> y obtén sorpresas, formación profesional y la oportunidad de subir tu CV que será visto por Nestlé y sus aliados.</p>
            </div>
            <div class="col-12 col-md-4 align-self-stretch align-items-center d-flex">
                <p class="text-second-section-2 text-md-left text-center">Iniciativa por los jóvenes te ofrece un espacio donde aprenderás habilidades para conseguir tu primer empleo a la vez que conoces los procesos de producción y distribución que Nestlé® lleva a cabo para que disfrutes de sus productos.</p>
            </div>
            <div class="col-12 col-md-4">
                
            </div>
            <div class="col-12 text-center p-4">
                <a href="#" class="btn-second-section mx-auto">Empieza tu ruta hacia el éxito y recibe sorpresas</a>
            </div>
            <a href="#third-section" class="arrow-section arrow-section-2"></a>
        </div>
        <div id="third-section" class="row align-items-center third-section">
            <?php echo $this->Html->image("/frontend/img/landing/banner-3.png", ['class' => 'img-fluid img-bg-3']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-3-mobile.png", ['class' => 'img-fluid img-bg-3-mobile']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-3.png", ['class' => 'img-fluid d-none d-md-flex']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-3-mobile.png", ['class' => 'img-fluid d-md-none', 'style="width:100%;"']); ?>
            <?php $step = $this->request->session()->read('Auth.User.step') ; 

                if (empty($step)) {
                    $step = 0;
                }
                echo $step;
             ?>
            <a href="/pages/juego" id="steps-1" class="steps steps-1 <?php if($step >= 1){ echo "active";}?>"></a>
            <a href="#steps-2" data-lity data-step="2" class="steps steps-2 <?php if($step >= 2){ echo "active";}?>"></a>
            <a href="#steps-3" data-lity data-step="3" class="steps steps-3 <?php if($step >= 3){ echo "active";}?>"></a>
            <a href="#steps-4" data-lity data-step="4" class="steps steps-4 <?php if($step >= 4){ echo "active";}?>"></a>
            <a href="#steps-5" data-lity data-step="5" class="steps steps-5 <?php if($step >= 5){ echo "active";}?>"></a>
            <a href="#steps-6" data-lity data-step="6" class="steps steps-6 <?php if($step >= 6){ echo "active";}?>"></a>
            <a href="#fourth-section" class="arrow-section arrow-section-3"></a>
        </div>
        <div id="fourth-section" class="row align-items-center fourth-section">
         <?php echo $this->Html->image("/frontend/img/landing/banner-4.jpg", ['class' => 'img-fluid img-bg-4']); ?>

            
            <div class="col-md-1"></div>
            <div class="col-12 col-md-4 bg-fourth-section align-self-stretch align-items-center d-flex px-5">
                <div class="text-center">
                    <h1 class="font-weight-bold">Noticias</h1>
                    <p>Conoce más sobre la Iniciativa por los Jóvenes en Venezuela</p>
                    <a href="#" class="btn-fourth-section mx-auto d-none d-md-flex">¿Quieres ver más noticias?</a>
                </div>
            </div>
            <div class="col-12 col-md-7">
                <a href="#" class="btn-fourth-section mx-auto d-md-none">¿Quieres ver más noticias?</a>
            </div>
            <a href="#fifth-section" class="arrow-section arrow-section-4"></a>
        </div>
        <div id="fifth-section" class="row align-items-lg-center align-items-start fifth-section">
         <?php echo $this->Html->image("/frontend/img/landing/banner-5.png", ['class' => 'img-fluid img-bg-5']); ?>
         <?php echo $this->Html->image("/frontend/img/landing/banner-5-mobile.jpg", ['class' => 'img-fluid img-bg-5-mobile']); ?>


            <div class="col-12 col-md-5 bg-fifth-section text-center text-md-left">
                <h1 class="font-weight-bold">Contáctanos</h1>
                <p>Queremos responder todas tus inquietudes</p>
                <table class="d-none d-md-flex">
                    <tr>
                        <th rowspan="2">
                        <?php echo $this->Html->image("/frontend/img/landing/phone-icon.png", ['style' => 'margin-left:-30px']); ?>                    
                        <th><a href="mailto:Servicio.Consumidor@ve.nestle.com">Servicio.Consumidor@ve.nestle.com</a></th>
                    </tr>
                        <tr>
                        <td>(0212) 0800 Nestlé 1 - (0800 637853 1)</td>
                    </tr>
                </table>
            </div>
            <div class="col-12 col-lg-7 d-md-none d-lg-flex"></div>
            <div class="col-12 col-lg-5 d-md-none d-lg-flex"></div>
            <div class="col-12 col-md-7 col-lg-7">
                <div class="second-fifth-section">
                    <table class="d-flex d-md-none">
                        <tbody class="mx-auto text-center">
                            <tr>
                                <th class="text-center">
                                <?php echo $this->Html->image("/frontend/img/landing/phone-icon.png"); ?>
                                
                                
                                </th>
                            </tr>
                            <tr>
                                <th style="text-align: center;"><a href="mailto:Servicio.Consumidor@ve.nestle.com">Servicio.Consumidor@ve.nestle.com</a></th>
                            </tr>
                            <tr>
                                <td>(0212) 0800 Nestlé 1 - (0800 637853 1)</td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="social-media text-center text-md-left">
                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    ¡Síguenos!</p>
                </div>
            </div>
        </div>
    </div>


<div id="steps-2" style="background-color: #fff; color: #333; width: 400px; height: 200px;" class="lity-hide">
    Lorem ipsum
</div>
<div id="steps-3" style="background-color: #fff; color: #333; width: 400px; height: 200px;" class="lity-hide">
    Lorem ipsum
</div>
<div id="steps-4" style="background-color: #fff; color: #333; width: 400px; height: 200px;" class="lity-hide">
    Lorem ipsum
</div>
<div id="steps-5" style="background-color: #fff; color: #333; width: 400px; height: 200px;" class="lity-hide">
    Lorem ipsum
</div>
<div id="steps-6" style="background-color: #fff; color: #333; width: 400px; height: 200px;" class="lity-hide">
    Lorem ipsum
</div>
