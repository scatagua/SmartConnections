
    <div class="container p-0">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light p-0 pl-lg-4 pt-lg-2 pb-lg-2 pt-2 pb-2 pl-3">
            <a class="navbar-brand mx-center" href="#">
                <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
            </a>
            
            <div class="navbar-collapse flex-row-reverse" id="navbarNavAltMarkup">
                <span class="navbar-text float-right">
                    <a class="d-block text-right mt-3 mt-lg-0" href="#">
                    <?php echo $this->Html->image("/frontend/img/empresas/ver-cv.png", ['class' => 'img-fluid']); ?>
                    </a>
                </span>
            </div>
        </nav>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 sections section-1">
                <div class="img-container">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-1.png", ['class' => 'd-block mx-auto icons']); ?>
               
                </div>
                <div class="d-block mx-auto sections-text">
                    <h3>Cómo hacer un CV atractivo</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat auctor nunc, laoreet dapibus nisl sodales non. Suspendisse vitae luctus tortor. Ut mollis nibh arcu, id scelerisque magna egestas et. Donec sit amet laoreet arcu. Etiam bibendum nisi eget feugiat semper. Duis dapibus lorem sed vehicula luctus.</p>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Juego</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-2">
                <div class="img-container">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-2.png", ['class' => 'd-block mx-auto icons']); ?>
                
                </div>
                <div class="d-block mx-auto sections-text">
                    <h3>El primer cara a cara</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat auctor nunc, laoreet dapibus nisl sodales non. Suspendisse vitae luctus tortor. Ut mollis nibh arcu, id scelerisque magna egestas et. Donec sit amet laoreet arcu. Etiam bibendum nisi eget feugiat semper. Duis dapibus lorem sed vehicula luctus.</p>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-3">
            <?php echo $this->Html->image("/frontend/img/empresas/icon-3.png", ['class' => 'd-block mx-auto icons']); ?>
                
                <div class="d-block mx-auto sections-text">
                    <h3>Potencia tu imagen personal con este e-learning</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat auctor nunc, laoreet dapibus nisl sodales non. Suspendisse vitae luctus tortor. Ut mollis nibh arcu, id scelerisque magna egestas et. Donec sit amet laoreet arcu. Etiam bibendum nisi eget feugiat semper. Duis dapibus lorem sed vehicula luctus.</p>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-4">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-4.png", ['class' => 'd-block mx-auto icons']); ?>
                <div class="d-block mx-auto sections-text">
                    <h3>Taller de comunicación asertiva/ liderazgo/ feedback</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat auctor nunc, laoreet dapibus nisl sodales non. Suspendisse vitae luctus tortor. Ut mollis nibh arcu, id scelerisque magna egestas et. Donec sit amet laoreet arcu. Etiam bibendum nisi eget feugiat semper. Duis dapibus lorem sed vehicula luctus.</p>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-5">
               <?php echo $this->Html->image("/frontend/img/empresas/icon-5.png", ['class' => 'd-block mx-auto icons']); ?>
                <div class="d-block mx-auto sections-text">
                    <h3>Conoce los componentes de las ofertas salariales</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat auctor nunc, laoreet dapibus nisl sodales non. Suspendisse vitae luctus tortor. Ut mollis nibh arcu, id scelerisque magna egestas et. Donec sit amet laoreet arcu. Etiam bibendum nisi eget feugiat semper. Duis dapibus lorem sed vehicula luctus.</p>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-6">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-6.png", ['class' => 'd-block mx-auto icons']); ?>
                <div class="d-block mx-auto sections-text">
                    <h3>Para vender tu chocolate es importante manejar tus finanzas</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat auctor nunc, laoreet dapibus nisl sodales non. Suspendisse vitae luctus tortor. Ut mollis nibh arcu, id scelerisque magna egestas et. Donec sit amet laoreet arcu. Etiam bibendum nisi eget feugiat semper. Duis dapibus lorem sed vehicula luctus.</p>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Main container -->
