<div class="container">

    <div class="row align-items-center justify-content-between">

        <div class="col-6 col-md-4">
            <img class="mt-2 img-fluid" src="http://www.montacargascaracas.com/nestle/talento-con-gusto-logo.png" alt="">
        </div>
        <div class="col-6 col-md-4">
            <span class="level">Nivel <?php echo $level ;?></span>
        </div>
        <div class="col-12 col-md-4">
            <img class="img-fluid logo-nestle" src="http://www.montacargascaracas.com/nestle/log-nestle.png" alt="">
        </div>

    </div>

    <div class="row justify-content-center mt-4 mb-4">
        <div class="col col-md-10">
            <div class="row">
                <div class="col">
                    <div class="grid">
                        <?php 
                            for ($i=1; $i < 5 ; $i++) { ?>
                                <div class="grid-item">
                                   
                                    <?php echo $this->Html->image("/frontend/img/juego/niveles/".$level."/".$i.".jpg", ['class' => 'rounded img-fluid']); ?>
                                </div>  
                         <?php   }
                        ?>
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div id="hold">
        </div>
        <div id="buttons">
        </div>
    </div>

    <div class="hombre">
        <img style="display:block" class="img-fluid persona neutral" src="http://www.montacargascaracas.com/nestle/hombre-neutral.png" alt="">
        <img style="display:none" class="img-fluid persona feliz" src="http://www.montacargascaracas.com/nestle/hombre-feliz.png" alt="">
        <img style="display:none" class="img-fluid persona triste" src="http://www.montacargascaracas.com/nestle/hombre-triste.png" alt="">
    </div>
    <div class="mujer">
        <img style="display:block" class="img-fluid persona neutral" src="http://www.montacargascaracas.com/nestle/mujer-neutral.png" alt="">
        <img style="display:none" class="img-fluid persona feliz" src="http://www.montacargascaracas.com/nestle/mujer-feliz.png" alt="">
        <img style="display:none" class="img-fluid persona triste" src="http://www.montacargascaracas.com/nestle/mujer-triste.png" alt="">
        <img id="hint" class="img-fluid hint" src="http://www.montacargascaracas.com/nestle/hint.png" alt="">
        <span class="nro-pista">2</span>
    </div>

</div>

<span style="display: none" id="word"><?php echo $palabras; ?></span>
<span style="display: none" id="level"><?php echo $level ;?></span>

