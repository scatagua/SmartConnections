<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nestle');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '20301671');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-vu1e+/Ym}|lRkaO^IrJ+NaOP[MBI:aIFTE{gk+?4gx/5cqylXoPeG5<o7rL0<a.');
define('SECURE_AUTH_KEY',  'Z0O&AK@!Dj)00atL@jG;LLjeUw~}3$+&*C$fw.X+fe8C7.>?BHimioK0jQEijO5:');
define('LOGGED_IN_KEY',    'I8.;CJ2a7GCbljEWFl8scgkH1vI,UYe);YIP^g[r?T~?hmSM+lxB?o_:+:H<Jj@6');
define('NONCE_KEY',        '?ZYk.cn,1xNEC:%y@xY)79*KxLzp{29X)xQK%Iuo9G5I!=aPNl`yA+@.fZ~lfkh@');
define('AUTH_SALT',        'mDG?GE`GU0yMAXHi:jxdotWaDK3+#5U*5[^26Gja:2ap>D`N%wa[H2|N?FyI>elG');
define('SECURE_AUTH_SALT', '[TE_t@Z]RY<YW=65vq@Dr.1;ZBFM1@cQU^5Uw*247$A^Y`RuNVN_9uA`i5mEghJQ');
define('LOGGED_IN_SALT',   'n9_IImi;:99d-mF<<_^*KS[F^#yo7Qur8fUnWg>0dt+3yhm8GPjp.`1{.o<?DuQ5');
define('NONCE_SALT',       'Wmc/gCZ.O,EXNt1EciHgSZdk@*hnx *T,PF<BiCX{tQqOUG[3N{7C[1aMmz,KxqX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
