$(function() {

	const mq = window.matchMedia( "(min-width: 768px)" );

	function calculateHeight(){
		if (mq.matches) {
			// window width is at least 768px
			for (i = 1; i < 6; i++) { 
			   switch(i) {
			       	case 1:
			       		var imgHeight = $( ".img-bg-1" ).height();
			       		var videoPlay = $( ".video-play" ).height();
           				$(".first-section").css('height', imgHeight);
           				$(".first-section").css('min-height', imgHeight);
           				$(".bg-first-section").css('height', videoPlay);
			           	break;
			       	case 2:
			           	var imgHeight = $( ".img-bg-2" ).height();
           				$(".second-section").css('height', imgHeight);
           				$(".second-section").css('min-height', imgHeight);
			          	break;
		        	case 3:
		            	var imgHeight = $( ".img-bg-3" ).height();
		            	$(".third-section").css('height', imgHeight);
		           		break;
   			       	case 4:
           				var imgHeight = $( ".img-bg-4" ).height();
           				$(".fourth-section").css('height', imgHeight);
   			          	break;
   		        	case 5:
   		            	var imgHeight = $( ".img-bg-5" ).height();
        				$(".fifth-section").css('height', imgHeight);
        				$(".fifth-section").css('min-height', imgHeight);
   		           		break;
			       	default:
			        	console.log(i)
			   }
			}
		} else {
			// window width is at less than 768px
			for (i = 1; i < 6; i++) { 
			   switch(i) {
			       	case 1:
			       		var imgHeight = $( ".img-bg-1-mobile" ).height();
			       		$(".first-section").css('min-height', imgHeight);
			       		$(".first-section").css('height', 'initial');
			       		$(".bg-first-section").css('height', 'initial');
			           	break;
			       	case 2:
			           	var imgHeight = $( ".img-bg-2-mobile" ).height();
           			  	$(".second-section").css('min-height', imgHeight);
           			  	$(".second-section").css('height', 'initial');
			          	break;
		        	case 3:
		            	$(".third-section").css('height', 'initial');
		           		break;
   			       	case 4:
           				$(".fourth-section").css('height', 'initial');
   			          	break;
   		        	case 5:
   		            	var imgHeight = $( ".img-bg-5-mobile" ).height();
        			   	$(".fifth-section").css('min-height', imgHeight);
        			  	$(".fifth-section").css('height', 'initial');
   		           		break;
			       	default:
			        	console.log(i)
			   }
			}
		  	
		}
	}

	Pace.on('done', function(){
		calculateHeight();	
	})	

	var resizeTimeout;
	$( window ).resize(function() {
		if(!!resizeTimeout){ clearTimeout(resizeTimeout); }
	  	resizeTimeout = setTimeout(function(){
		    calculateHeight();
	  	},200);
	});

	var $root = $('html, body');
	$('.navbar-nav a, .arrow-section').click(function() {
	    $root.animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 500);
	    return false;
	});

	$(".steps-3, .steps-4, .steps-5, .steps-6").on('click', function(e){
		$(this).addClass("active");
		$.ajax({
		    url: 'user/steps',
		    type: 'POST',
		    data: { paso:e.target.attributes[2].nodeValue}
		})
		.done(function() {
		    
		});
	});

});