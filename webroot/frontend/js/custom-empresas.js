$(function() {

	function calculateWidth(){
		for (i = 1; i < 7; i++) { 
		   switch(i) {
		       	case 1:
		       		var width = $(".section-"+i+" img.icons").width();
		       		$(".section-"+i+" .sections-text").css("width", width);
		           	break;
		       	case 2:
		       		var width = $(".section-"+i+" img.icons").width();
		       		$(".section-"+i+" .sections-text").css("width", width);
		          	break;
	        	case 3:
	        		var width = $(".section-"+i+" img.icons").width();
		       		$(".section-"+i+" .sections-text").css("width", width);
	        		break;
		       	case 4:
		       		var width = $(".section-"+i+" img.icons").width();
		       		$(".section-"+i+" .sections-text").css("width", width);
		          	break;
	        	case 5:
	        		var width = $(".section-"+i+" img.icons").width();
		       		$(".section-"+i+" .sections-text").css("width", width);
	           		break;
	           	case 6:
	        		var width = $(".section-"+i+" img.icons").width();
		       		$(".section-"+i+" .sections-text").css("width", width);
	           		break;
		       	default:
		        	console.log(i)
		   }
		}
		calculateMaxHeight();
	}

	function calculateMaxHeight(){
		var t=0; // the height of the highest element (after the function runs)
		var t_elem;  // the highest element (after the function runs)
		$(".sections-text").each(function () {
		    $this = $(this);
		    if ( $this.outerHeight() > t ) {
		        t_elem=this;
		        t=$this.outerHeight();
		    }
		});

		var height = $(t_elem).height();
		$(".sections-text").css("min-height", height);
	}

	Pace.on('done', function(){
		calculateWidth();
	})	

	var resizeTimeout;
	$( window ).resize(function() {
		if(!!resizeTimeout){ clearTimeout(resizeTimeout); }
	  	resizeTimeout = setTimeout(function(){
		    calculateWidth();
	  	},200);
	});

});