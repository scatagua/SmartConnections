window.onload = function() {

    var alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
        't', 'u', 'v', 'w', 'x', 'y', 'z'
    ];

    var wordToGuess = $("#word").html();

    var categories; // Array of topics
    var chosenCategory; // Selected catagory
    var getHint; // Word getHint
    var hints; // Number of hints
    var hintPosition; // Hint position in array
    var word; // Selected word
    var wordLength; // Selected word
    var guess; // Geuss
    var storedGuesses = []; // Stored geusses
    var lives; // Lives
    var counter; // Count correct geusses
    var space; // Number of spaces in word '-'
    var level; // Set Level
    var videoStatus; // Set which video to play
    var instance;
    var nextLevel;

    // Get elements
    var showLives = document.getElementById("mylives");
    var showCatagory = document.getElementById("scatagory");
    var getHint = document.getElementById("hint");
    var showClue = document.getElementById("clue");

    var hintPosition = 0;
    var hints = 2;
    var level = $("#level").html();
    var videoStatus = 'start';
    var nextLevel = false;

    // declare an array compare method
    Array.prototype.compare = function(array) {
        if (!array) {
            return false;
        }
        if (this.length !== array.length) {
            return false;
        }
        for (var i = 0, l = this.length; i < l; i++) {
            if (this[i] instanceof Array && array[i] instanceof Array) {
                if (!this[i].compare(array[i])) {
                    return false;
                }
            } else if (this[i] !== array[i]) {
                return false;
            }
        }
        return true;
    }

    // create an array shuffle function
    function shuffle(a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    }

    // create alphabet ul
    var buttons = function() {

        myButtons = document.getElementById('buttons');
        letters = document.createElement('ul');

        // turn word string into array
        wordToGuess = wordToGuess.split('');

        // If word is less than 18 character then add the missing with random characters
        if (wordToGuess.length < 18) {
            alphabet = alphabet.filter(function(val) {
                return wordToGuess.indexOf(val) == -1;
            });
            shuffle(alphabet);
            var missingWords = 18 - wordToGuess.length;
            for (var i = 0; i < missingWords; i++) {
                wordToGuess.push(alphabet[i]);
            }
        }

        // randomize word array
        shuffle(wordToGuess);

        for (var i = 0; i < wordToGuess.length; i++) {
            letters.id = 'alphabet';
            list = document.createElement('li');
            list.id = 'letter';
            list.innerHTML = wordToGuess[i];
            check();
            myButtons.appendChild(letters);
            letters.appendChild(list);
        }
    }

    // Create geusses ul
    result = function() {
        wordHolder = document.getElementById('hold');
        correct = document.createElement('ul');

        for (var i = 0; i < word.length; i++) {
            correct.setAttribute('id', 'my-word');
            guess = document.createElement('li');
            guess.setAttribute('class', 'guess');
            if (word[i] === "-") {
                guess.innerHTML = "-";
                space = 1;
            } else {
                guess.innerHTML = "_";
            }

            storedGuesses.push(guess);
            wordHolder.appendChild(correct);
            correct.appendChild(guess);
        }
    }

    // OnClick Function
    check = function() {
        list.onclick = function() {

            if (counter !== wordLength) {
                var geuss = (this.innerHTML);
                this.setAttribute("class", "active");
                this.onclick = null;

                for (var i = 0; i < word.length; i++) {
                    var index = storedGuesses[i].innerHTML.indexOf("_");
                    if (index !== -1) {
                        storedGuesses[i].innerHTML = geuss;
                        counter += 1;
                        break;
                    }
                }

                checkWord();
            }
        }
    }

    // Play
    play = function() {

        word = wordToGuess;
        wordLength = word.length;
        //word = word.replace(/\s/g, "-");
        buttons();

        storedGuesses = [];
        lives = 10;
        counter = 0;
        space = 0;
        result();
    }

    play();

    // Hint
    hint.onclick = function() {

        if (hintPosition <= 1) {

            hints -= 1;
            $(".nro-pista").html(hints);

            if (storedGuesses[hintPosition].innerHTML === "_") {
                counter += 1;
            }

            if (storedGuesses[hintPosition].innerHTML !== "_") {
                $("#alphabet li.active").each(function(index) {
                    if (this.innerHTML === storedGuesses[hintPosition].innerHTML) {
                        $(this).removeClass("active");
                        $(this).bind('click', function() {
                            var geuss = (this.innerHTML);
                            this.setAttribute("class", "active");
                            $(this).off("click");

                            for (var i = 0; i < word.length; i++) {
                                var index = storedGuesses[i].innerHTML.indexOf("_");
                                if (index !== -1) {
                                    storedGuesses[i].innerHTML = geuss;
                                    counter += 1;
                                    break;
                                }
                            }
                        });
                        return false;
                    }
                });
            }

            storedGuesses[hintPosition].innerHTML = word[hintPosition];
            $(storedGuesses[hintPosition]).addClass("definitive");
            $(storedGuesses[hintPosition]).css("color", "#22B573");
            $("#alphabet li").each(function(index) {
                if (this.innerHTML === word[hintPosition]) {
                    $(this).addClass("active");
                    this.onclick = null;
                    return false;
                }
            });

            hintPosition += 1;
            checkWord();

        }
    }

    // Restore words
    $("#my-word li").on('click', function(e) {
        var definitive = $(this).hasClass("definitive");
        if (!definitive) {
            if (e.target.innerHTML !== "_") {
                var letterToRestore = e.target.innerHTML;
                e.target.innerHTML = "_";
                $("#alphabet li.active").each(function(index) {
                    if (this.innerHTML === letterToRestore) {
                        $(this).removeClass("active");
                        $(this).bind('click', function() {
                            var geuss = (this.innerHTML);
                            this.setAttribute("class", "active");
                            $(this).off("click");

                            for (var i = 0; i < word.length; i++) {
                                var index = storedGuesses[i].innerHTML.indexOf("_");
                                if (index !== -1) {
                                    storedGuesses[i].innerHTML = geuss;
                                    counter += 1;
                                    break;
                                }
                            }
                        });
                        return false;
                    }
                });
                counter -= 1;
                checkWord();
            }
        }
    });

    // Check for correct word
    checkWord = function() {
        if (counter + space === wordLength) {
            console.log(counter)
            var result = [];
            $(storedGuesses).each(function() {
                result.push(this.innerHTML);
            });
            result = result.join("");
            if (result === word) {

                videoStatus = "next";
                $("#letter").off("click");
                $("#my-word").css("color", "#22B573");
                $(".hombre .neutral, .mujer .neutral").css("display", "none");
                $(".hombre .feliz, .mujer .feliz").css("display", "block");

                $.ajax({
                    url: '/pages/juegolevel',
                    type: 'POST',
                    data: { level:level }
                })
                .done(function() {
                    setTimeout(function(){
                        launchLightBox();
                        loadPlayer();
                        if(level!=="25"){
                            nextLevel = true;
                        }
                    }, 2000)
                });

            } else {
                $("#my-word").css("color", "#C1272D");
                $(".hombre .neutral, .mujer .neutral").css("display", "none");
                $(".hombre .triste, .mujer .triste").css("display", "block");
            }
        } else {
            $("#my-word").css("color", "#B3B3B3");
            $(".hombre .neutral, .mujer .neutral").css("display", "block");
            $(".hombre .triste, .mujer .triste").css("display", "none");
        }
    }

    // packery load
    $('.grid').packery({
        itemSelector: '.grid-item',
        percentPosition: true
    });

    // Youtube Player

    $(document).ready(function() {
        if (level === "1"){
            launchLightBox();
            loadPlayer();
        }
    });

    const mq = window.matchMedia( "(min-width: 880px)" );

    function getVideo() {

        if (level === "25"){
            videoStatus = 'won';
        } else if (level === "final"){
            videoStatus = 'final';
        }
        if (videoStatus === 'start'){
            return '9kUz5wq_qjA';
        } else if (videoStatus === 'next'){
            return 'Kw-gXPXEnqg';
        } else if (videoStatus === 'won'){
            return 'osS5OnyyhjQ';
        } else if (videoStatus === 'final'){
            level = "termino";
            return '_s8N4ATImgY';
        }

    }

    function getWidth(){
        if (mq.matches) {
            // window width is at least 880px
            return '880';
        } else {
            // window width is at less than 880px
            return '100%';
        }
    }

    function loadPlayer() {
        if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            window.onYouTubePlayerAPIReady = function() {
                onYouTubePlayer();
            };

        } else {

            onYouTubePlayer();

        }
    }

    var player;

    function onYouTubePlayer() {
        player = new YT.Player('player', {
            height: '490',
            width: getWidth(),
            videoId: getVideo(),
            playerVars: { 'controls': 0, 'showinfo': 0, 'rel': 0, 'showsearch': 0, 'iv_load_policy': 3, 'autoplay': 1, 'modestbranding': 1, 'disablekb': 1 },
            events: {
                'onStateChange': onPlayerStateChange,
                'onError': catchError,
                'onReady': onPlayerReady
            }
        });
    }

    var done = false;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            // setTimeout(stopVideo, 6000);
            done = true;
        } else if (event.data == YT.PlayerState.ENDED) {
            // location.reload();
            instance.close().then(function() {
                console.log('Lightbox closed');
                if(nextLevel){
                    location.reload();
                } else if(level==="25"){
                    level = "final";
                    launchLightBox();
                    loadPlayer();
                } else if( level === "termino"){
                    window.location.href = '../users/registro_talento';
                }
            });
        }
    }

    function onPlayerReady(event) {
        var embedCode = event.target.getVideoEmbedCode();
        event.target.playVideo();
        if (document.getElementById('embed-code')) {
        document.getElementById('embed-code').innerHTML = embedCode;
        }
    }

    function catchError(event) {
        if (event.data == 100) console.log("Error");
    }

    function stopVideo() {
        player.stopVideo();
    }

    //Lightbox for videos

    function launchLightBox(){
        instance = lity('<div id="player"></div>');
        instance.options('esc', false);
    }

}
